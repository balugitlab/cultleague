class AssociateDivisionsAndUsers < ActiveRecord::Migration[6.0]
  def change
    create_join_table :divisions, :users
  end
end
