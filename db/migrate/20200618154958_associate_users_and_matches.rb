class AssociateUsersAndMatches < ActiveRecord::Migration[6.0]
  def change
    create_join_table :matches, :users
  end
end
