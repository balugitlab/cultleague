Rails.application.routes.draw do
  devise_for :users

  resources :rules
  resources :shedules
  
  resources :matches do
    collection do
      get "mine"
    end

    resources :scores

  end

  resources :seasons do
    resources :divisions do
      # TODO: add matches and scores as nested routes
      member do
        get "join"
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'home#index'
end
