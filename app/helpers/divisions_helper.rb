module DivisionsHelper

  def get_top_division_players(division)
    # returns @wins with the best player at index 0
    # and user_losses
    # both type hash

    users = division.users

    @user_wins = Hash.new
    @user_loses = Hash.new

    users.each do |user|
      @user_wins[user] = 0
      @user_loses[user] = 0

      matches = user.matches.where(division_id: division.id)

      matches.each do |match|

        match.scores.where(user_id: user.id).each do |score|
          @user_wins[user] += score.wins
          @user_loses[user] += score.loses
        end

      end

    end

    @wins = @user_wins.sort_by {|_key, value| value}.reverse

  end
end
