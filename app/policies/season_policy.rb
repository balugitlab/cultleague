class SeasonPolicy < ApplicationPolicy


  attr_reader :user, :season

  def initialize(user, season)
    @user = user
    @season = season
  end

  def create?
    user.mod_role? || user.admin_role?
  end

  def update?
    user.mod_role? || user.admin_role?
  end

  def destroy?
    user.mod_role? || user.admin_role?
  end

end
