class ScoresController < ApplicationController

  # whole thing is a bit glued togheter and assumes certain behaviour but should work for now

  before_action :set_score, only: [:show, :edit, :update, :destroy]

  # GET /scores
  # GET /scores.json
  def index
    @scores = Score.all
  end

  # GET /scores/1
  # GET /scores/1.json
  def show
  end

  # GET /scores/new
  def new
    @match = Match.find(params[:match_id])
    @score = Score.new
  end

  # GET /scores/1/edit
  def edit
    @match = Match.first

  end

  # POST /scores
  # POST /scores.json
  def create

    @match = Match.find(params[:match_id])
    @score = Score.new(score_params)
    @score.match_id = @match.id
    @score.user_id = helpers.current_user.id

    @oponent_score = Score.new
    @oponent_score.match_id = @match.id
    @oponent_score.user_id = @match.users.where.not(id: helpers.current_user.id).first.id
    # invert the loses and wins
    @oponent_score.wins = @score.loses
    @oponent_score.loses =  @score.wins

    if not @score.save! or not @oponent_score.save!
      render 'new'
      return
    end

    redirect_to mine_matches_path()
  end

  # PATCH/PUT /scores/1
  # PATCH/PUT /scores/1.json
  def update
    respond_to do |format|
      if @score.update(score_params)
        format.html { redirect_to @score, notice: 'Score was successfully updated.' }
        format.json { render :show, status: :ok, location: @score }
      else
        format.html { render :edit }
        format.json { render json: @score.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scores/1
  # DELETE /scores/1.json
  def destroy
    @score.destroy
    respond_to do |format|
      format.html { redirect_to scores_url, notice: 'Score was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_score
      @score = Score.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def score_params
      params.require(:score).permit(:wins, :loses)
    end
end
