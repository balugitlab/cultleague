class HomeController < ApplicationController
  def index

    @season = Season.last
    if @season == nil
      # no season exist creating default one
      @season = Season.create(name: "First Season")
    end


  end
end
