class MatchesController < ApplicationController
  before_action :authenticate_user!, only: [:mine]

  def index
    # display recent matches
    # order by score created at
    @matches = Match.where(id: Score.select("match_id")).includes(:scores).order('scores.created_at desc').limit(10)

  end

  def mine

    @matches = helpers.current_user.matches.order(created_at: :desc).limit(150)

  end

  def show
  end

  def edit
  end

  def update
  end
end
