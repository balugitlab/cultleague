class DivisionsController < ApplicationController
  before_action :set_season_division, only: [:show] # can this be deleted?
  before_action :authenticate_user!, only: [:join]


  def show

  end

  def new
    @season = Season.find(params[:season_id])
    @division = Division.new
  end

  def create
    @season = Season.find(params[:season_id])
    @division = @season.divisions.create(divisions_params)
    redirect_to season_path(@season)
  end

  def edit
    @season = Season.find(params[:season_id])
    @division = @season.divisions.find(params[:id])
  end

  def update
    @season = Season.find(params[:season_id])
    @division = @season.divisions.find(params[:id])

    if @division.update(divisions_params)
      redirect_to @season
    else
      render 'edit'
    end
  end

  def join

    # Joins the user to the divisionn as a player
    @season = Season.find(params[:season_id])
    @division = @season.divisions.find(params[:id])

    # check if user has already joined
    if @division.users.exists?(helpers.current_user.id)

      flash[:notice] = "Already Joined"
      redirect_to @season
      return
    end

    # join user to the division
    @division.users << helpers.current_user

    # matchmake the user
    @division.users.each do |user|

      next if user == helpers.current_user

      @match = @division.matches.create()
      @match.users << user
      @match.users << helpers.current_user
    end

    flash[:notice] = "Successfully Joined"
    redirect_to @season

  end

  def destroy
    @season = Season.find(params[:season_id])
    @division = @season.divisions.find(params[:id])
    @division.destroy
    redirect_to season_path(@season)
  end

  private

  def set_season_division
    @season = Season.find(params[:season_id])
    @division = @season.divisions.find(params[:id])

  end
    def divisions_params
      params.require(:division).permit(:name)
    end

end
