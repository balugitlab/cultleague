class SeasonsController < ApplicationController

  def index
    @seasons = Season.all()
  end

  def show
    @season = Season.find(params[:id])
  end

  def new
    @season = Season.new
  end

  def create

    @season = Season.new(season_params)
    # authorize @season

    if @season.save
      redirect_to @season
    else
      render 'new'
    end
  end

  def edit
    @season = Season.find(params[:id])
  end

  def update
    @season = Season.find(params[:id])

    if @season.update(season_params)
      redirect_to @season
    else
      render 'edit'
    end
  end

  def destroy

    # TODO: role based auth

    @season = Season.find(params[:id])
    @season.destroy
    redirect_to seasons_path
  end

  private
  def season_params
    params.require(:season).permit(:name)
  end
end
